
<html>
<style>
    .container {
        text-align: center;
        display: table-cell;

    }
    .content {
        text-align: center;
        display: inline-block;
    }
    .h2_cls{
        color: #4cae4c;
    }
    .body_cls{
        background-color: #888a85;
    }
</style>
<head>
    <h2 class="h2_cls">USER INFORMATION</h2>
</head>


<form  id="user_form" name="user_form" method="post" action="{{URL::to('edit-user-detail')}}">

    {{csrf_field()}}
    <input type="hidden" name="userid" id="userid" value="{{@$show_user_det->id}}">
    <div class="md--rw">
        {{--@if (count($errors) > 0)--}}
        {{--<div class="alert alert-danger">--}}
        {{--<ul>--}}
        {{--@foreach ($errors->all() as $error)--}}
        {{--<li>{{ $error }}</li>--}}
        {{--@endforeach--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--@endif--}}
        <label for="first_name">First Name</label>
        <input  type="text" id="first_name" name="first_name" value="{{@$show_user_det->firstname}}" >
       {{ $errors->first('first_name') }}
        <label for="middle_name">Middle Name</label>
        <input type="text" id="middle_name" name="middle_name" value="{{@$show_user_det->middlename}}" >
        <label  for="last_name">Last Name</label>
        <input type="text" id="last_name" name="last_name"  value="{{@$show_user_det->lastname}}">
        {{ $errors->first('last_name') }}
    </div>
    <div class="md--rw">
        <div class="md--rw">
            <label  for="email">Email</label>
            <input type="text" id="email" name="email" value="{{@$show_user_det->email}}">
            {{ $errors->first('email')}}
            <label for="mobile">Mobile</label>
            <input type="text" id="mobile" name="mobile"  maxlength="15" value="{{@$show_user_det->mobile}}">
            {{$errors->first('mobile')}}

        </div>
    </div>
    <div>
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored" type="submit" >
            <i class="material-icons" >Edit</i>
        </button>
    </div>
</form>
<p><a href="{{URL::to('show-user')}}">BACK</a></p>


<div class="main_loader" style="display: none">
    <div class="mdl-spinner mdl-js-spinner is-active"></div>
</div>



</html>