
<html>
<style>
    .container {
        text-align: center;
        display: table-cell;

    }
    .content {
        text-align: center;
        display: inline-block;
    }
    .h2_cls{
        color: #4cae4c;
    }
    .body_cls{
        background-color: #888a85;
    }
</style>
<head>
    <h2 class="h2_cls">USER INFORMATION</h2>
</head>


    <form  id="user_form" name="user_form" method="post" action="{{URL::to('save-user-detail')}}">

        {{csrf_field()}}
        <input type="hidden" name="id" id="id">
        <div class="md--rw">
            {{--@if (count($errors) > 0)--}}
                {{--<div class="alert alert-danger">--}}
                    {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--@endif--}}
            <label for="first_name">First Name</label>
            <input  type="text" id="first_name" name="first_name" value="{{old('first_name')}}" >
                {{ $errors->first('first_name') }}
            <label for="middle_name">Middle Name</label>
            <input type="text" id="middle_name" name="middle_name" >
            <label  for="last_name">Last Name</label>
            <input type="text" id="last_name" name="last_name"  value="{{old('last_name')}}">
            {{ $errors->first('last_name') }}
        </div>
        <div class="md--rw">
            <div class="md--rw">
            <label  for="email">Email</label>
            <input type="text" id="email" name="email" value="{{old('email')}}">
                {{ $errors->first('email')}}
            <label for="mobile">Mobile</label>
            <input type="text" id="mobile" name="mobile"  maxlength="15" value="{{old('mobile')}}">
                {{$errors->first('mobile')}}

            </div>
        </div>
        <div  class="md--rw">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored" type="submit" >
            <i class="material-icons" >SAVE</i>
        </button>
        </div>
       </form>


    <div id="success_dialog" style="display: none;">
        <div class="cd-popup is-visible">
            <div class="cd-popup-container">
                <h4 class="mdl-dialog__title" >Success!!</h4>
                <div class="mdl-dialog__content">
                        <p style="color: green;"> Added successfully.</p>
                </div>
                <div class="mdl-dialog__actions">
                    <button type="submit" onclick="reload_page();" class="mdl-button cd_close cancel_btn">OK</button>
                </div>
            </div>
        </div>
    </div>

<p><a href="{{URL::to('show-user')}}">View list of users</a></p>
    <div class="main_loader" style="display: none">
        <div class="mdl-spinner mdl-js-spinner is-active"></div>
    </div>



</html>