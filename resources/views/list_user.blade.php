<html>
<head>
    <h3>LIST OF USERS</h3>
</head>
<body>
<form name="list_form" id="list_form" method="post" >
<table id="list_user" border="1%"  bgcolor="#faebd7">
    <tr>
        <td>ID</td>
        <td>First Name</td>
        <td>Middle Name</td>
        <td>Last Name</td>
        <td>Email</td>
        <td>Phone</td>
        <td>Edit</td>
        <td>Delete</td>
    </tr>
    <?php $count = 1; ?>
    @foreach(@$show_user as $show_users)
        <tr>
            <td>{{@$count}}</td>
            <td>{{@$show_users->firstname}}</td>
            <td>{{@$show_users->middlename}}</td>
            <td>{{@$show_users->lastname}}</td>
            <td>{{@$show_users->email}}</td>
            <td>{{@$show_users->mobile}}</td>
            <td><a href="{{URL::to('edit-user/'.$show_users->id)}}">EDIT</a></td>
            <td><a href="{{URL::to('delete-user/'.$show_users->id)}}">DELETE</a></td>
        </tr>
        <?php $count++; ?>
        @endforeach
</table>
    @if(Session::has('flash_message'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
    @endif

</form>
</body>
</html>