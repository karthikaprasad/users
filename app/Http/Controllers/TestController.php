<?php

namespace App\Http\Controllers;
use App\userInformation;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;

class TestController extends Controller
{
    /* */
    public function addUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:120',
            'last_name' => 'required|max:120',
            'email' => 'required|email|unique:user_info',
            'mobile' => 'required|min:11|numeric',

        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }


            $user_details = new userInformation();

            if($user_details) {
                $user_details->firstname = $request->input('first_name');
                $user_details->lastname = $request->input('last_name');
                $user_details->middlename = $request->input('middle_name');
                $user_details->email = $request->input('email');
                $user_details->mobile = $request->input('mobile');
                $user_details->save();
//                $request->session()->flash('alert-success', 'User was successful added!');
                \Session::flash('flash_message','successfully saved.');
                return redirect('show-user');



            }
            else
            {
                echo "error";
            }
    }
    public function showUsers()
    {

        $show_user = userInformation::get();
       return view('list_user',compact('show_user'));
    }
    public function editUser($userid)
    {

        $show_user_det = userInformation::find($userid);
        return view('edit_user',compact('show_user_det'));
    }
    public function editDataUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:120',
            'last_name' => 'required|max:120',
            'email' => 'required|email',
            'mobile' => 'required|min:11|numeric',

        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

           $user_id_value=$request->input('userid');
        if($user_id_value) {

            $user_details = userInformation::find($user_id_value);
            $user_details->firstname = $request->input('first_name');
            $user_details->lastname = $request->input('last_name');
            $user_details->middlename = $request->input('middle_name');
            $user_details->email = $request->input('email');
            $user_details->mobile = $request->input('mobile');
            $user_details->save();
            \Session::flash('flash_message','successfully edited.');
            return redirect('show-user');



        }
        else
        {
            echo "error";
        }
    }
    public function deleteUser($id)
    {

        $show_users = userInformation::find($id);
        $show_users->delete();
        \Session::flash('flash_message','successfully deleted.');
        return redirect('show-user');

    }
}
