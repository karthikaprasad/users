<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Front Page
Route::get('/', function () {

   return view('add_edit_user');
});

//For Save
Route::post('save-user-detail','TestController@addUser');

//List OF users
Route::get('show-user','TestController@showUsers');


//Edit user
   Route::get('edit-user/{id}', 'TestController@editUser');

   //Edit
Route::post('edit-user-detail','TestController@editDataUser');

//Delete
Route::get('delete-user/{id}','TestController@deleteUser');